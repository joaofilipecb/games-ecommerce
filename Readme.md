﻿# Cubos - The Movies

Desenvolvimento de aplicação (front-end) para o teste da ConsultaRemedios. A aplicação consiste em: ***listar*** os games na página, ***Adicionar*** e ***remover*** os itens do carrinho, ordenar os produtos por preço, popularidade e ordem alfabética.

### Bibliotecas utilizadas
 - node-sass 
 - react-redux 
 - react-router-dom 
 - redux
 - redux-thunk

### Instalação e iniciação
Após ter dado um clone, a partir do repositório: https://bitbucket.org/joaofilipecb/the-movie/, entre no diretório da aplicação e execute os seguintes comandos:

 - `npm install`

 - `npm start`