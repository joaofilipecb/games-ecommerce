import { combineReducers } from "redux";

const cart = (state = { data: false }, action) => {
    switch (action.type) {
        case "ADD_CART_ITEM":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};

const orderGames = (state = { data: false }, action) => {
    switch (action.type) {
        case "ORDER_GAMES":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};


export default combineReducers({ cart, orderGames });
