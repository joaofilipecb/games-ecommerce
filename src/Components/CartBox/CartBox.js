import React, { useEffect, useState } from 'react';
import './CartBox.scss';
import cartIcon from './img/cart-icon.svg';
import { useSelector } from 'react-redux';

function CartBox() {

    const [itemCart, setItemCart] = useState({ products: [], references: [] });
    const [totalCart, setTotalCart] = useState();
    const itemCartReducer = useSelector(state => state.cart.data)

    useEffect(() => {

        // SE O DADO DO REDUCER FOR VERDADEIRO, CHAMAMOS A FUNÇÃO QUE ADICIONA OS ITENS NO CARRINHO
        itemCartReducer && addItemToCart()

    }, [itemCartReducer])

    useEffect(() => {
        itemCart.products.length > 0 && sumTotalCart()
    }, [itemCart])


    const addItemToCart = () => {

        // VERIFICA SE O ID DO GAME JÁ EXISTE NO ESTADO DO CARRINHO

        itemCart.references.indexOf(itemCartReducer.id) === -1 && (
            setItemCart({
                products: [...itemCart.products, itemCartReducer], references: [...itemCart.references, itemCartReducer.id]
            })
        )


    }

    const sumTotalCart = () => {

        // FAZ A SOMA TOTAL DOS PRODUTOS DO CARRINHO

        let sum = [];
        itemCart.products.map((product) => sum = [...sum, product.price])
        let sumReduce = sum.reduce((a, b) => a + b)
        sumReduce >= 250 ? setTotalCart({ total: sumReduce, freight: 0 }) : setTotalCart({ total: sumReduce, freight: sum.length * 10 })

    }

    const removeItemCart = (productStatus) => {

        // REMOVE OS ITENS DO CARRINHO

        let remove = { product: [], references: [...itemCart.references] };

        itemCart.products.map((product) => {
            product.id !== productStatus.id && (remove.product = [...remove.product, product]);
        })

        let index = remove.references.indexOf(productStatus.id);
        index !== -1 && remove.references.splice(index, 1);

        setItemCart({ products: remove.product, references: remove.references });

    }


    return (

        <div className="cart-box">
            <h2>Carrinho</h2>
            {!itemCart.products || itemCart.products.length === 0 ?
                <>
                    <img className="cart-icon" src={cartIcon}></img>
                    <p className="desc">Até o momento, <br />o seu carrinho está vazio</p>
                </>
                :
                <div className="items">
                    {itemCart.products.map((product, i) =>
                        <div key={i} className="item">
                            <div className="top-item">
                                <img src={require('../../assets/' + product.image)} />
                            </div>
                            <div className="bottom-item">
                                <h4>{product.name}</h4>
                                <p className="price">R$ {product.price}</p>
                            </div>
                            <span onClick={() => removeItemCart(product)} className="remove">X</span>
                        </div>
                    )}
                    <div className="payment-information">
                        <div className="total"><p>subtotal</p><h4>R$ {totalCart && totalCart.total}</h4></div>
                        <div className="total"><p>frete</p><h4>R$ {totalCart && totalCart.freight}</h4></div>
                        <div className="total"><p>total</p><h4 className="total-value">R$ {totalCart && totalCart.total + totalCart.freight}</h4></div>
                        <button>finalizar compra</button>
                    </div>
                </div>
            }
        </div>
    );
}


export default CartBox;