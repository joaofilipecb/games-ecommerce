import React, { useEffect, useState } from 'react';
import './Games.scss';
import productsRequest from '../../products.json';
import { useDispatch, useSelector } from 'react-redux';
import { addCartItem } from '../../actions/index';

function Games() {

    const dispatch = useDispatch();
    const [products, setProducts] = useState(productsRequest);
    const orderGamesReducer = useSelector(state => state.orderGames.data);

    useEffect(() => {
        
        // VERIFICA SE O REDUCE É FALSO OU VERDADEIRO E PASSA COMO PARAMETRO O TIPO DE ORDENAÇÃO A SER EXECUTADO

        orderGames(!orderGamesReducer ? 'score': orderGamesReducer)

    }, [orderGamesReducer])

    const addToCart = (product) => {
        
        dispatch(addCartItem(product));

    }

    const orderGames = (orderType) => {

        // ORDENAÇÃO

        let items = [...products]

        items.sort((a, b) => {

            switch (orderType) {
                case 'a-z':

                    if (a.name < b.name) return -1
                    else if (a.name > b.name) return 1
                    else return 0

                case 'z-a':

                    if (a.name > b.name) return -1
                    else if (a.name < b.name) return 1
                    else return 0

                case 'price':

                    if (Math.round(a.price) < Math.round(b.price)) return -1
                    else if (Math.round(a.price) > Math.round(b.price)) return 1
                    else return 0

                case 'score':

                    if (a.score > b.score) return -1
                    else if (a.score < b.score) return 1
                    else return 0
                    
            }

        })

        setProducts(items)

    }

    return (
        <>{
            products.map((product, i) =>
                <div key={i} className="game margin-box">
                    <div className="gray-picture">
                        <img src={require('../../assets/' + product.image)} />
                    </div>
                    <div className="info-bottom">
                        <div className="transition">
                            <h3>{product.name}</h3>
                            <p>R$ {product.price}</p>
                            <button onClick={() => addToCart(product)}>adicionar ao carrinho</button>
                        </div>
                    </div>
                </div>
            )
        }
        </>
    );
}


export default Games;