import React from 'react';
import '../GlobalAssets/GlobalAssets.scss';
import FilterGames from '../FilterGames/FilterGames';
import CartBox from '../CartBox/CartBox';
import Games from '../Games/Games';

function BoxAll() {

    return (
        <div className="box-all">
            <div className="flex-side">
                <div className="b-1 p-r-20">
                    <div className="flex-side flex-align-center flex-space-between height-header">
                        <h1>Games</h1>
                        <FilterGames></FilterGames>
                    </div>
                    <Games></Games>
                </div>
                <div className="b-2">
                    <CartBox></CartBox>
                </div>
            </div>
        </div>
    );
}


export default BoxAll;