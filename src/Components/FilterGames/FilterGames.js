import React from 'react';
import './FilterGames.scss';
import {useDispatch} from 'react-redux';
import { orderGames } from '../../actions/index';

function FilterGames() {
    const dispatch = useDispatch();
    return (
        <select onChange={(e)=> dispatch(orderGames(e))} className="filter-games">
            <option value="score">Mais populares</option>
            <option value="price">Preço</option>
            <option value="a-z">A - Z</option>
            <option value="z-a">Z - A</option>
        </select>
  );
}

export default FilterGames;