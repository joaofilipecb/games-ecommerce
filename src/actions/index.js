
export const addCartItem = (item) => {
    return {
        type: 'ADD_CART_ITEM',
        data: item
    }
}

export const orderGames = (order) => {
    return {
        type: 'ORDER_GAMES',
        data: order.currentTarget.value
    }
}